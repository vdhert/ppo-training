from abc import ABC
from abc import abstractmethod


class Pet(ABC):
    @abstractmethod
    def __init__(self):
        name = "Balbinka"

    def set_name(self, name):
        self.name = name

    @abstractmethod
    def talk(self):
        pass


class Cat(Pet):
    def __init__(self):
        super().__init__()

    def talk(self):
        print("mrr")


class Dog(Pet):
    def __init__(self):
        self.name = "Burek"

    def talk(self):
        print("bark")


class MadDog(Dog):
    def __init__(self):
        super().__init__()

    def talk(self):
        self.foam_at_the_mouth()
        super().talk()

    def foam_at_the_mouth(self):
        print("*foams at mouth*")
