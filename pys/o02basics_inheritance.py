from abc import ABC
from abc import abstractmethod


class Base(ABC):
    @abstractmethod
    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name


class SubclassA(Base):
    def __init__(self, name):
        super().__init__(name)

    def class_a_method(self):
        return [ord(character) for character in self.name]


class SubclassB(Base):
    def __init__(self, name):
        Base.__init__(self, name)

    def class_b_method(self, arg1=42):
        return SubclassB(self.name + str(arg1))
