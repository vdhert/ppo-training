class BasicClass:
    def __init__(
        self, public_string_attribute, protected_int_attribute, private_float_attribute
    ):
        self.public_string_attribute = public_string_attribute
        self._protected_int_attribute = protected_int_attribute
        self.__private_float_attribute = private_float_attribute

    def public_method_wo_arguments(self):
        return 42 * self._protected_int_attribute

    def public_method_taking_int_returning_float(self, arg1):
        return 42.0 * self.__private_float_attribute * arg1
