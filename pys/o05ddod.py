class Base:
    def method(self):
        print("Base method")


class A(Base):
    def method(self):
        super().method()
        print("A method")


class B(Base):
    def method(self):
        super().method()
        print("B method")


class F(Base):
    def method(self):
        super().method()
        print("F method")


class C(A):
    def method(self):
        super().method()
        print("C method")


class D(B):
    def method(self):
        super().method()
        print("D method")


class E(C, D):
    def method(self):
        super().method()
        print("E method")
