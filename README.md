# Object-oriented programming in Python

# Table of Contents
* [What are we going to talk about?](#What-are-we-going-to-talk-about?)
* [Why OOP](#Why-OOP)
* [Design](#Design)
* [Classes](#Classes)
* [SOLID](#SOLID)
* [Methods: abstract, static, class](#Methods:-abstract,-static,-class)
* [OOP-related things in Python](#OOP-related-things-in-Python)

I assume you know basics of Python:
* assigning and using variables
* using basic iterables as tuples, lists and dicts
* `for`- and `while`-loops
* `if`-statements
* writing and running scripts
* importing modules and objects from modules

## What are we going to talk about?
* OOP
* Python
* ~~in~~ (OOP in Python)

## Why OOP

* Python is (might be) OOL: imperative, interpreted with dynamic typing
* non-OOP has simple types, arrays and structures
* Simula 67
* example `calculate_area`
* objects, inheritance, polymorphism

## Design

* big subject on its own
* UML:
    * class with attributes and methods
    * types of attributes
    * returned types: simple, array, other class

![](UMLs/001basics.png)

## Classes

* basics: `class`, methods, `__init__`(without comments for now)
* what methods can do (encapsulation etc.)

    ![](UMLs/002basics-inheritance.png)
* inheritance:
    * motivation -- reuse code, DRY-WET
    * base class superclass subclass
    * methods extension and overwriting

    ![](UMLs/004inheritance.png)
    * single and multiple inheritance -- MRO, DDoD

    ![](UMLs/005DDoD.png)
* creation (design patterns)
    * factory

    ![](UMLs/003factory.png)
    * builder
* [task] bees
* access:
    * public
    * protected
    * private -- `__` in Python
    * [task] ?

## SOLID

* low coupling, high cohesion
* single responsibility principle -- naming convention ftr
* open-close principle: classes should be open for extension, closed for modification
* Liskov substitution principle
* interface segregation principle
* dependency inversion principle

## Methods: abstract, static, class

* abstract classes
* [task] update bees
* static methods
* class methods
* [task] document settings in latex
* decorators + scopes, partial
* magic:
    * init -- not a constructor [option to talk about metaclasses]
    * repr, str
    * lt, gt etc.
    * iter -- generators, consumers
    * dict and slots

## OOP-related things in Python

* try-except
* context, with-statement
